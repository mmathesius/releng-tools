#!/usr/bin/env groovy

pipeline{
    agent {label "baremetal"}

    options{
        timeout(time: 360, unit: 'MINUTES')
        gitLabConnection('gitlab')
    }

    environment {
        KEYTAB = credentials('releng-ops-centos-stream-keytab')
        CCCC_SSH_KEY = credentials('releng-ops-releng-jenkins-sshkey')
    }

    stages {

        stage('Setup'){
            steps {
                deleteDir()
                checkout scm
                sh('kinit -k -t $KEYTAB -p "centos-stream@REDHAT.COM"')
            }
        }

        stage('Run Stream 9 CCCC'){
            steps {
                withCredentials([string(credentialsId: 'gitlab-token', variable: 'TOKEN')]) {
                    sh """
                    scripts/stream9_cccc.sh
                    """
                }
            }
        }
    }

    post {
        success {
            script {
                def pub_jenkins_url = "https://jenkins.stream.centos.org/"
                def build_link = pub_jenkins_url.concat(env.BUILD_URL.substring(env.BUILD_URL.indexOf("/job")+1))
                def message = "The pipeline ran successfully. Please look at the job details at " + build_link
                addGitLabMRComment(comment: message)
            }
        }

        failure{ 
            script {
                def pub_jenkins_url = "https://jenkins.stream.centos.org/"
                def build_link = pub_jenkins_url.concat(env.BUILD_URL.substring(env.BUILD_URL.indexOf("/job")+1))
                def message = "The pipeline ran failed. Please look at the job details at " + build_link
                addGitLabMRComment(comment: message)
            }
        }
    }
}

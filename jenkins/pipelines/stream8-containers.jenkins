
arches = ['amd64', 'arm64v8', 'ppc64le'] as Set
imagepushorg = 'quay.io/centos'
manifestpushrepo = 'quay.io/centos/centos'
imagetags = ['stream', 'stream8'] as Set

for (arch in arches) {
    node(arch){
        stage(arch) {
            stage('Checkout') {
                checkout(
                        scm: [
                        $class: 'GitSCM',
                        userRemoteConfigs: [[
                        url: 'https://github.com/CentOS/sig-cloud-instance-images'
                        ]],
                        extensions: [
                            [$class: 'SparseCheckoutPaths',  sparseCheckoutPaths:[[$class:'SparseCheckoutPath', path:'docker/']]],
                            [$class: 'CleanBeforeCheckout'], 
                            [$class: 'CloneOption', depth: 1, noTags: false, reference: '', shallow: true, timeout: 10]
                        ],
                        branches: [[ name: '*/CentOS-8-Stream']],
                        ],
                        )
            }

            stage('Pull UBI') {
                sh '''podman pull registry.access.redhat.com/ubi8:latest'''
            }

            stage('Build') {
                sh '''podman build --format docker -t localhost/centos-stream:latest docker/'''
            }

            stage('Tag') {
                for (tag in imagetags) {
                    withEnv(['TAG='+tag, 'ARCH='+arch, 'IMAGEPUSHORG='+imagepushorg]){
                        sh '''podman tag localhost/centos-stream:latest ${IMAGEPUSHORG}/${ARCH}:${TAG}'''
                    }
                }
            }

            stage('Taginfo') {
                for (tag in imagetags) {
                    withEnv(['TAG='+tag, 'ARCH='+arch, 'IMAGEPUSHORG='+imagepushorg]){
                        sh '''#!/usr/bin/env bash
                            set -Eeuo pipefail
                            mkdir -p build-info/image-ids
                            podman inspect --format '{{ .Digest }}' ${IMAGEPUSHORG}/${ARCH}:${TAG} | tee build-info/${ARCH}-${TAG}.txt
                            '''
                    }
                }

                archiveArtifacts 'build-info/**'
            }

            stage('Push') {
                for (tag in imagetags) {
                    withEnv(['TAG='+tag, 'ARCH='+arch, 'IMAGEPUSHORG='+imagepushorg]){
                        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'releng-ops-quay-login', usernameVariable: 'QUAY_PUSH_USERNAME', passwordVariable: 'QUAY_PUSH_PASS']]){
                            sh '''podman login -u=${QUAY_PUSH_USERNAME} -p ${QUAY_PUSH_PASS} quay.io'''
                            sh '''podman push ${IMAGEPUSHORG}/${ARCH}:${TAG}'''
                        }
                   }
                }
            }
        }
    }
}

node('amd64'){
    stage('Create Manifests'){
        for (tag in imagetags) {
            withEnv(['MANIFESTPUSHREPO='+manifestpushrepo, 'TAG='+tag, 'IMAGEPUSHORG='+imagepushorg]){
                withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'releng-ops-quay-login', usernameVariable: 'QUAY_PUSH_USERNAME', passwordVariable: 'QUAY_PUSH_PASS']]){
                    sh '''podman login -u=${QUAY_PUSH_USERNAME} -p ${QUAY_PUSH_PASS} quay.io'''
                    try{
                        sh '''podman rmi ${MANIFESTPUSHREPO}:${TAG}'''
                    } catch (err) {
                    }
                    sh '''podman manifest create ${MANIFESTPUSHREPO}:${TAG}'''
                }
            }
        }
    }
    stage('Annotate Manifests'){
        for (tag in imagetags) {
            for (arch in arches) {
                withEnv(['MANIFESTPUSHREPO='+manifestpushrepo, 'TAG='+tag, 'ARCH='+arch, 'IMAGEPUSHORG='+imagepushorg]){
                    sh '''podman manifest add ${MANIFESTPUSHREPO}:${TAG} docker://${IMAGEPUSHORG}/${ARCH}:${TAG}'''
                }
            }
        }
    }
    stage('Push Manifests'){
        for (tag in imagetags) {
            withEnv(['MANIFESTPUSHREPO='+manifestpushrepo, 'TAG='+tag]){
                sh '''podman manifest push -f v2s2 --purge --all ${MANIFESTPUSHREPO}:${TAG} docker://${MANIFESTPUSHREPO}:${TAG}'''
            }
        }
    }
}


#!/usr/bin/bash

set -euo pipefail
IFS=$'\n\t'

if [[ $USER != 'compose' ]]; then
    echo "This script must be run as the compose user."
    exit 1
fi

COMPOSE="latest-CentOS-Stream"
CENTOS_RELEASE="9-stream"

DRY_RUN=$1

function message() {
    echo -e "\e[36m* $1\e[m"
}


function sync_rpms() {
    arch=$1
    shift
    variants=($@)
    for variant in ${variants[@]}; do
        VARIANT_DIR=/mnt/centos/staged/$CENTOS_RELEASE/$variant/$arch/
        if [ $DRY_RUN ]; then
            rsync -avhH --info progress2 --link-dest=/mnt/centos --include="os/images" --exclude="images/" --exclude="iso/" --dry-run \
                /mnt/centos/composes/production/$COMPOSE/compose/$variant/$arch/ /mnt/centos/staged/$CENTOS_RELEASE/$variant/$arch/
        else
            message "syncing $variant $arch"
            # delete old comps files first to ensure we use the new ones
            rm -vf ${VARIANT_DIR}/os/repodata/*-comps-*.xml*
            # rsync repo from compose
            rsync -avhH --info progress2 --link-dest=/mnt/centos --include="os/images" --exclude="images/" --exclude="iso/" \
                /mnt/centos/composes/production/$COMPOSE/compose/$variant/$arch/ ${VARIANT_DIR}/
            # modular metadata must be decompressed for createrepo_c to pick it up
            # https://github.com/rpm-software-management/createrepo_c/issues/263
            pushd /mnt/centos/staged/$CENTOS_RELEASE/$variant/$arch/os
            find repodata -type f -name '*-modules.yaml.gz' -exec gunzip -vfk {} \;
            find repodata -type f -name '*-modules.yaml.xz' -exec unxz -vfk {} \;
            # update repodata
            createrepo_c --update --workers 8 --xz \
                --distro 'cpe:/o:centos-stream:centos-stream:9,CentOS Stream 9' \
                --revision 9-stream \
                --groupfile $(find repodata -type f -name '*-comps-*.xml') \
                --retain-old-md-by-age 1m \
                .
            popd
        fi
    done
}


function sync_isos() {
    arch=$1
    shift
    variants=($@)
    for variant in ${variants[@]}; do
        VARIANT_DIR=/mnt/centos/staged/$CENTOS_RELEASE/$variant/$arch/iso/
        if [ $DRY_RUN ]; then
            rsync -avhH --info progress2 --dry-run --delete \
                /mnt/centos/composes/production/$COMPOSE/compose/$variant/$arch/iso/ $VARIANT_DIR
        else
            message "syncing $arch isos"
            # rsync iso directory from compose
            rsync -avhH --info progress2 --delete \
               /mnt/centos/composes/production/$COMPOSE/compose/$variant/$arch/iso/ $VARIANT_DIR
            # set up latest symlinks
            pushd $VARIANT_DIR
            for f in *.iso*; do
                datenum="$(echo $f |cut -d'-' -f4)"
                ln -sf $f $(echo $f | sed "s/$datenum/latest/");
            done
            popd
        fi
    done
}

function sync_source_rpms() {
    variants=($@)
    for variant in ${variants[@]}; do
        pushd /mnt/centos/staged/$CENTOS_RELEASE/$variant/source/tree/
        if [ $DRY_RUN ]; then
            rsync -avhH --info progress2 --link-dest=/mnt/centos --dry-run \
                /mnt/centos/composes/production/$COMPOSE/compose/$variant/source/tree/ ./
        else
            message "syncing source $variant"
            rsync -avhH --info progress2 --link-dest=/mnt/centos \
                /mnt/centos/composes/production/$COMPOSE/compose/$variant/source/tree/ ./
            createrepo_c --update --workers 8 --xz .
        fi
        popd
    done
}

pushd ${HOME} # We're running as the compose user so we want to be in a CWD that we can go back and forth to

sync_rpms aarch64 BaseOS AppStream CRB HighAvailability 
sync_rpms ppc64le BaseOS AppStream CRB HighAvailability ResilientStorage
sync_rpms s390x BaseOS AppStream CRB HighAvailability ResilientStorage
sync_rpms x86_64 BaseOS AppStream CRB HighAvailability NFV RT ResilientStorage

sync_source_rpms BaseOS AppStream CRB HighAvailability ResilientStorage RT

sync_isos aarch64 BaseOS
sync_isos ppc64le BaseOS
sync_isos s390x BaseOS
sync_isos x86_64 BaseOS

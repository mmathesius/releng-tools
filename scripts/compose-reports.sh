#!/bin/bash -e
# Generate reports on the Stream composes.
# This is roughly a jenkins version of ./cron-compose-reporting.sh

# git clone github.com:CentOS/sync2git.git

# mount the perm. storage

# link it up to ~/.local/compose

# for i in c8s c9s c9s-dev rhel8 rhel9
# ./compose.py latest $i

# ./compose.py check
# ./compose.py clean

# Check to see if the latest has changed:
# ./compose.py latest $i

# If changed:
# ./compose.py diff $i

# If either changes: c8s rhel8 | c9s rhel9
# ./compose.py diff x y
